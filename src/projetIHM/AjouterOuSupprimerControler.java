package projetIHM;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class AjouterOuSupprimerControler {

	@FXML
	private Button ajouter;

	@FXML
	private Button supprimer;

	@FXML
	void ajouter(ActionEvent event) {

	}

	@FXML
	void supprimer(ActionEvent event) {

	}

	static Stage popUp = new Stage();
	
	@FXML
	void initialize() {
		ajouter.setOnMouseClicked(event -> {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("Ajout.fxml"));
			Parent root;
			try {
				root = (Parent) loader.load();
				Scene scene = new Scene(root);
				popUp.setScene(scene);
				popUp.setTitle("Ajouter");
				popUp.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			Stage s = (Stage) ajouter.getScene().getWindow();
			s.close();
		});
		
		supprimer.setOnMouseClicked(event -> {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("Supprimer.fxml"));
			Parent root;
			try {
				root = (Parent) loader.load();
				Scene scene = new Scene(root);
				popUp.setScene(scene);
				popUp.setTitle("Ajouter");
				popUp.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			Stage s = (Stage) supprimer.getScene().getWindow();
			s.close();
		});
	}
}
