package projetIHM;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AjoutControler {
	@FXML
    private Button valider;

    @FXML
    private Button annuler;

    @FXML
    private TextField tfY;

    @FXML
    private TextField tfX;

    @FXML
    private TextField nom;
    
    @FXML
    void ajouter(ActionEvent event) {
    	Controler.ajouter(nom.getText(),Integer.parseInt(tfX.getText()),Integer.parseInt(tfY.getText()));
    	Stage s = (Stage) valider.getScene().getWindow();
		s.close();
    }

    @FXML
    void quitter(ActionEvent event) {
    	Stage s = (Stage) annuler.getScene().getWindow();
		s.close();
    }
 
}
