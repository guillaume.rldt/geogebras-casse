package projetIHM;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class CompositionController {

    @FXML
    private TextField hX;

    @FXML
    private TextField hY;

    @FXML
    private CheckBox r;

    @FXML
    private TextField rD;

    @FXML
    private CheckBox t;

    @FXML
    private TextField tX;

    @FXML
    private TextField tY;

    @FXML
    private TextField rX;

    @FXML
    private TextField rY;

    @FXML
    private CheckBox h;

    @FXML
    private ComboBox<?> nom;

    @FXML
    private Button annuler;
    
    @FXML
    private Button valider;
    
    @FXML
    private TextField echelle;
    
    void initialize() {
    	nom = new ComboBox<String>(FXCollections.observableArrayList(Controler.getObjet()));
    }
    
    @FXML
    void faire(ActionEvent event) {
    	if(t.isSelected()) Controler.doTranslation((String)nom.getValue(), Double.parseDouble(tX.getText()), Double.parseDouble(tY.getText()));
    	if(r.isSelected()) Controler.doRotation((String)nom.getValue(), Double.parseDouble(rD.getText()), Double.parseDouble(rX.getText()), Double.parseDouble(rY.getText()));
    	if(h.isSelected()) Controler.doHomothetie((String)nom.getValue(),Double.parseDouble(echelle.getText()), Double.parseDouble(hX.getText()), Double.parseDouble(hY.getText()));
    	Stage s = (Stage) valider.getScene().getWindow();
		s.close();
    }

    @FXML
    void quitter(ActionEvent event) {
    	Stage s = (Stage) annuler.getScene().getWindow();
		s.close();
    }
}

