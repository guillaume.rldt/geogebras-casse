package projetIHM;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import demo.LibraryException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import transforms.*;
import transforms.mobile.Motif;
import transforms.élémentaires.Homothetie;
import transforms.élémentaires.Rotation;
import transforms.élémentaires.Transformation;
import transforms.élémentaires.Translation;


public class Controler {

	@FXML
	private Button undo;

	@FXML
	private Button redo;

	@FXML
	private Button matrice;

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private Button boutonRotation;

	@FXML
	private Button boutonTranslation;

	@FXML
	private Button carre;

	@FXML
	private Button boutonHomothetie;

	@FXML
	private Button boutonCompostion;

	@FXML
	private Button cercle;

	@FXML
	private Pane pane;

	@FXML
	private Button triangle;

	@FXML
	void colorEnteredCercle(MouseEvent event) {
		cercle.setStyle("-fx-background-color: white");
	}

	@FXML
	void colorExitedCercle(MouseEvent event) {
		cercle.setStyle("-fx-background-color: lightgrey");
	}

	@FXML
	void colorEnteredTriangle(MouseEvent event) {
		triangle.setStyle("-fx-background-color: white");
	}

	@FXML
	void colorExitedTriangle(MouseEvent event) {
		triangle.setStyle("-fx-background-color: lightgrey");
	}

	@FXML
	void colorEnteredCarre(MouseEvent event) {
		carre.setStyle("-fx-background-color: white");
	}

	@FXML
	void colorExitedCarre(MouseEvent event) {
		carre.setStyle("-fx-background-color: lightgrey");
	}

	@FXML
	void colorEnteredCompostion(MouseEvent event) {
		boutonCompostion.setStyle("-fx-background-color: white");
	}

	@FXML
	void colorExitedCompostion(MouseEvent event) {
		boutonCompostion.setStyle("-fx-background-color: lightgrey");
	}

	@FXML
	void colorEnteredTranslation(MouseEvent event) {
		boutonTranslation.setStyle("-fx-background-color: white");
	}

	@FXML
	void colorExitedTranslation(MouseEvent event) {
		boutonTranslation.setStyle("-fx-background-color: lightgrey");
	}

	@FXML
	void colorEnteredRotation(MouseEvent event) {
		boutonRotation.setStyle("-fx-background-color: white");
	}

	@FXML
	void colorExitedRotation(MouseEvent event) {
		boutonRotation.setStyle("-fx-background-color: lightgrey");
	}

	@FXML
	void colorEnteredHomothetie(MouseEvent event) {
		boutonHomothetie.setStyle("-fx-background-color: white");
	}

	@FXML
	void colorExitedHomothetie(MouseEvent event) {
		boutonHomothetie.setStyle("-fx-background-color: lightgrey");
	}

	@FXML
	void ajout(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("Ajout.fxml"));
		Parent root = (Parent) loader.load();

		Scene scene = new Scene(root);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle("FXML demo");
		stage.show();
	}

	private static IComposition composition;
	private List<Node> allNodes;
	private static ArrayList<String> objet = new ArrayList<String>();
	private static ArrayList<Boolean> display = new ArrayList<Boolean>();

	static Stage popUp = new Stage();

	@FXML
	void initialize() {
		composition = new Composition();
		/**
        composition.add(new Translation((Double.parseDouble(coordX.getText())), (Double.parseDouble(coordY.getText()))));
        composition.add(new Rotation(Double.parseDouble(angle.getText()), 0.0, 0.0));
        composition.add(new Homothetie(Double.parseDouble(echelle.getText()),0.0,0.0));
		 **/

		pane.getChildren().add(composition.getGrille(pane));
		//allNodes = composition.draw(display);
		//pane.getChildren().addAll(allNodes);

		triangle.setOnMouseClicked(event -> {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("ajouterOuSupprimer.fxml"));
			Parent root;
			try {
				root = (Parent) loader.load();
				Scene scene = new Scene(root);
				popUp.setScene(scene);
				popUp.setTitle("Triangle");
				popUp.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		});

		cercle.setOnMouseClicked(event -> {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("ajouterOuSupprimer.fxml"));
			Parent root;
			try {
				root = (Parent) loader.load();
				Scene scene = new Scene(root);
				popUp.setScene(scene);
				popUp.setTitle("Cercle");
				popUp.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		});

		carre.setOnMouseClicked(event -> {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("ajouterOuSupprimer.fxml"));
			Parent root;
			try {
				root = (Parent) loader.load();
				Scene scene = new Scene(root);
				popUp.setScene(scene);
				popUp.setTitle("Carre");
				popUp.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		});

		boutonCompostion.setOnMouseClicked(event -> {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("Composition.fxml"));
			Parent root;
			try {
				root = (Parent) loader.load();
				Scene scene = new Scene(root);
				popUp.setScene(scene);
				popUp.setTitle("Carre");
				popUp.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		});
	}

	static void ajouter(String name, double x , double y) {
		if(!objet.contains(name)) {
			composition.add(new Translation(x, y));
			objet.add(name);
			display.add(true);
		}
		else System.err.println("Nom deja utiliser");
	}

	static void supprimer(String name) {
		if(!objet.contains(name)) {
			int index = getIndex(name,objet);
			display.remove(index);
			display.add(index,false);
			objet.remove(name);
			objet.add(index,null);	
		}
		else System.err.println("Nom n'est encore utiliser");
	}

	public static int getIndex(String name, ArrayList<String> objetList) {
		for(int i = 0; i < objetList.size(); i++) {
			if(name.equals(objetList.get(i))) {
				return i;
			}
		}
	}

	public void doMatrices(ActionEvent actionEvent) {
		try {
			matrice.setText(Arrays.deepToString(composition.getAtomicMatrix(1))+"\n" +composition.getComposedMatrix(2)); 
		} catch (LibraryException e) {
			e.printStackTrace();
		}
	}

	public static void doTranslation(String name, double x, double y) {
		composition.add(new Translation(x, y));
	}

	public static void doRotation(String name, double degres, double x,double y) {
		composition.add(new Rotation(degres, x, y));  
	}

	public static void doHomothetie(String string, double echelle, double x, double y) {
		composition.add(new Homothetie(echelle,x , y));
	}

	public static ArrayList<String> getObjet(){
		ArrayList <String> nonNull = new ArrayList<String>();
		for (int i = 0; i < objet.size(); i++) {
			if (!objet.get(i).equals(null)) nonNull.add(objet.get(i));
		}
		return nonNull;
	}

	@FXML
	private Label coordonner;


	public void showCoord(MouseEvent mouseEvent) {
		double xMouse = mouseEvent.getX();
		double yMouse = mouseEvent.getY();
		coordonner.setText("X vaut : " + composition.xMouseToMath(xMouse) + " Y vaut : " + composition.yMouseToMath(yMouse));
	}

	@FXML
	void unShowCoord(ActionEvent event) {
		coordonner.setText("Coordonner");
	}

}
